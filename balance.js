function Bugs(objectName) {
    this.name = objectName;
    this.weight = 0;

    this.setWeight = function(num) {
        this.weight = num;
    }

    this.getName = function() {
        return this.name;
    }

    this.getWeight = function() {
        return this.weight;
    }

}
var setleft = new Set();
var setrigth = new Set();

var bugs = [];
window.onload = function() {
    var sum = 0;
    for (var i = 1; i <= 12; i++) {
        bugs[i] = new Bugs('bugs' + i);
        bugs[i].setWeight(getRandomInt(1, 10));
        sum += bugs[i].getWeight();
    }
    while (sum % 2 != 0) {
        sum = 0;
        for (var i = 1; i <= 12; i++) {
            bugs[i].setWeight(getRandomInt(1, 10));
            sum += bugs[i].getWeight();
        }
    }

    for (var i = 1; i <= 12; i++) {
        gravity(bugs[i].getName(), i * 100 + i, 300, 750);

    }
    console.log(sum);
};

function offset(el) {
    var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return {
        top: rect.top + scrollTop,
        left: rect.left + scrollLeft
    }
}

function gravity(name, posX, posY, bottom) {
    avatar = document.getElementById(name);
    avatar.style.zIndex = 9999;
    avatar.style.top = posY + "px";
    newX = posX;
    avatar.style.left = newX + "px";
    while (parseInt(avatar.style.top.substring(-2)) <= bottom) {
        parseInt(avatar.style.top.substring(-2))
        newY = parseInt(avatar.style.top.substring(-2)) + 5;
        avatar.style.top = newY + "px";
        avatar.style.position = 'absolute';
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var DragManager = new function() {

    var dragObject = {};

    var self = this;

    function onMouseDown(e) {

        if (e.which != 1) return;

        var elem = e.target.closest('.draggable');
        if (!elem) return;

        dragObject.elem = elem;
        dragObject.weight = bugs[dragObject.elem.id.substr(4)].getWeight()
        dragObject.name = bugs[dragObject.elem.id.substr(4)].getName();

        dragObject.downX = e.pageX;
        dragObject.downY = e.pageY;

        return false;
    }

    function onMouseMove(e) {
        if (!dragObject.elem) return;

        if (!dragObject.avatar) {
            var moveX = e.pageX - dragObject.downX;
            var moveY = e.pageY - dragObject.downY;

            if (Math.abs(moveX) < 3 && Math.abs(moveY) < 3) {
                return;
            }

            dragObject.avatar = createAvatar(e);
            if (!dragObject.avatar) {
                dragObject = {};
                return;
            }

            var coords = getCoords(dragObject.avatar);
            dragObject.shiftX = dragObject.downX - coords.left;
            dragObject.shiftY = dragObject.downY - coords.top;

            startDrag(e);
        }

        dragObject.avatar.style.left = e.pageX - dragObject.shiftX + 'px';
        dragObject.avatar.style.top = e.pageY - dragObject.shiftY + 'px';

        return false;
    }

    function onMouseUp(e) {
        if (dragObject.avatar) {
            finishDrag(e);
        }

        dragObject = {};
    }

    function finishDrag(e) {
        dragObject.avatar.rollback();
    }

    function createAvatar(e) {

        var avatar = dragObject.elem;
        var old = {
            parent: avatar.parentNode,
            nextSibling: avatar.nextSibling,
            position: avatar.position || '',
            left: avatar.left || '',
            top: avatar.top || '',
            zIndex: avatar.zIndex || ''
        };

        avatar.rollback = function() {
            if (parseInt(dragObject.avatar.style.left.substring(-2)) > 100 && parseInt(dragObject.avatar.style.left.substring(-2)) < 700 && parseInt(dragObject.avatar.style.top.substring(-2)) < 310) {
                gravity(dragObject.name, dragObject.avatar.style.left, dragObject.avatar.style.top, 310)
                document.getElementById('leftnum').innerHTML = parseInt(document.getElementById('leftnum').innerHTML) + dragObject.weight;
                setleft.add(dragObject.name);
            } else if (parseInt(dragObject.avatar.style.left.substring(-2)) > 1100 && parseInt(dragObject.avatar.style.left.substring(-2)) < 1700 && parseInt(dragObject.avatar.style.top.substring(-2)) < 310) {
                gravity(dragObject.name, dragObject.avatar.style.left, dragObject.avatar.style.top, 310)
                document.getElementById('rigthnum').innerHTML = parseInt(document.getElementById('rigthnum').innerHTML) + dragObject.weight;
                setrigth.add(dragObject.name);
            } else {
                gravity(dragObject.name, dragObject.avatar.style.left, dragObject.avatar.style.top, 750)
            }
            old.parent.insertBefore(avatar, old.nextSibling);

            if (setleft.size + setrigth.size == 12 && parseInt(document.getElementById('leftnum').innerHTML) == parseInt(document.getElementById('rigthnum').innerHTML)) {
                alert("Вы выиграли!");
            }

        };
        return avatar;
    }

    function startDrag(e) {
        var avatar = dragObject.avatar;
        if (setleft.delete(dragObject.name)) {
            document.getElementById('leftnum').innerHTML = parseInt(document.getElementById('leftnum').innerHTML) - dragObject.weight;
        }
        if (setrigth.delete(dragObject.name)) {
            document.getElementById('rigthnum').innerHTML = parseInt(document.getElementById('rigthnum').innerHTML) - dragObject.weight;
        }
        document.body.appendChild(avatar);
        avatar.style.zIndex = 9999;
        avatar.style.position = 'absolute';
    }

    document.onmousemove = onMouseMove;
    document.onmouseup = onMouseUp;
    document.onmousedown = onMouseDown;

    this.onDragEnd = function(dragObject, dropElem) {};
    this.onDragCancel = function(dragObject) {};

};


function getCoords(elem) { // кроме IE8-
    var box = elem.getBoundingClientRect();
    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
    };

}
